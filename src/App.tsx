import React, { useState } from 'react';
import { useEffect } from 'react';
import generateMessage, {
  Message
} from './Api';

const App: React.FC<{}> = () => {
  const [err, setErr] = useState<Message[]>([]);
  const [warning, setWarning] = useState<Message[]>([]);
  const [info, setInfo] = useState<Message[]>([]);
  const [start, setStart] = useState<boolean>(true);

  useEffect(() => {
    if (start) {
      generateMessage((message: Message) => {
        if (message.priority === 0)
          setErr(err => [message, ...err]);
        else if (message.priority === 1)
          setWarning(warning => [message, ...warning]);
        else
          setInfo(info => [message, ...info]);
      })
    }
  }, [start]);


  const handleStart = () => {
    setStart(start => !start)
  }


  const handleReset = () => {
    setErr([]);
    setWarning([]);
    setInfo([]);
  }



  return (
    <div>
      <div style={{ border: "3px solid Black" }}>
        <h3 style={{ marginLeft: "150px" }}>Coding Chellange</h3>
      </div>
      <div style={{ border: "3px solid Black", borderTop: "0px" }}>
        <div style={{ display: "flex", justifyContent: 'center', paddingTop: "7px" }}>
          <button style={{ background: "#88FCA3" }} onClick={handleStart}>{start ? "STOP" : "START"}</button>
          <button style={{ background: "#88FCA3" }} onClick={handleReset}>CLEAR</button>
        </div>
        <div style={{ display: "flex", justifyContent: 'space-around' }}>
          <div style={{ width: "500px" }}>
            <h2>Error Type 1</h2>
            <h4>Count: {err.length}</h4>
            <ul style={{ listStyle: "none", overflowY: "scroll", height: "600px" }}>
              {err.length ? err.map((item) => (
                <div style={{ backgroundColor: "#F56236", marginTop: "5px", height: "50px", borderRadius: "5px", display: "flex", justifyContent: 'center', alignItems: "center" }}>{item.message}</div>
              )) : ''}
            </ul>
          </div>
          <div style={{ width: "500px" }}>
            <h2>Warning Type 2</h2>
            <h4>Count: {warning.length}</h4>
            <ul style={{ listStyle: "none", overflowY: "scroll", height: "600px" }}>
              {warning.length ? warning.map((item) => (
                <div style={{ backgroundColor: "#FCE788", marginTop: "5px", height: "50px", borderRadius: "5px", display: "flex", justifyContent: 'center', alignItems: "center" }}>{item.message}</div>
              )) : ''}
            </ul>
          </div>
          <div style={{ width: "500px" }}>
            <h2>Info Type 3</h2>
            <h4>Count: {info.length}</h4>
            <ul style={{ listStyle: "none", overflowY: "scroll", height: "600px" }}>
              {info.length ? info.map((item) => (
                <div style={{ backgroundColor: "#88FCA3", marginTop: "5px", height: "50px", borderRadius: "5px", display: "flex", justifyContent: 'center', alignItems: "center" }}>{item.message}</div>
              )) : ''}
            </ul>
          </div>
        </div >
      </div>
    </div>
  );
}

export default App;
